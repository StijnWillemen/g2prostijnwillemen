﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bmih5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef uw gewicht");

            int gewicht = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("geef uw lengte in m");
            double lengte = Convert.ToDouble(Console.ReadLine());
            double bmi = gewicht / (lengte * lengte);
            
            Console.WriteLine("uw bmi bedraagt " + Math.Round(bmi, 2));
            if(bmi<18.5)
                {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(("Ondergewicht"));
                Console.ResetColor();
            }
            else if(bmi>18.5 && bmi<25)
            {
               
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Goed gewicht");
                Console.ResetColor();
            }
            else if(bmi>25 && bmi<40)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("overgewicht");
                Console.ResetColor();
            }
            else if(bmi>40)
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("Zwaar obesitas");
                Console.ResetColor();
            }

            Console.ReadKey();
        }
    }
}
