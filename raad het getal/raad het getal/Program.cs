﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace methodenOefeningen
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("1:RaadHetGetal");
            Console.WriteLine("2:SchaarSteenPapier");
            int keuze = Convert.ToInt32(Console.ReadLine());
            switch (keuze)
            {
                case 1:
                    RaadHetGetal();
                    break;
                case 2:
                    SchaarSteenPapier();
                    break;
            }
        }
        static void RaadHetGetal()
        {
            int getal;
            int poging = 0;
            string pogingString;
            bool gevonden = false;
            Random rand = new Random();
            getal = rand.Next(0, 100);
            int ondergrens = 0;
            int bovengrens = 100;
            int pogingen = 0;

            while (!gevonden)
            {
                Console.WriteLine($"Geef een getal tussen {ondergrens} en {bovengrens}");
                pogingString = Console.ReadLine();
                poging = int.Parse(pogingString);
                pogingen++;
                if (getal > poging)
                {
                    Console.WriteLine("Het gezochte getal is groter, probeer opnieuw.");
                    ondergrens = poging;
                }
                else if (getal < poging)
                {
                    Console.WriteLine("Het gezochte getal is kleiner, probeer opnieuw.");
                    bovengrens = poging;
                }
                else
                    gevonden = true;
            }

            Console.WriteLine($"Gevonden! Het te zoeken getal was inderdaad {getal} je had er {pogingen} pogingen voor nodig.");
            Console.ReadLine();
        }





        static void SchaarSteenPapier()
        {
            Console.WriteLine("eerste tot hoeveel?");
            int max = Convert.ToInt32(Console.ReadLine());
            int scoreH = 0;
            int scorePC = 0;
            while (scoreH!=max && scorePC!=max)
            {
                Console.WriteLine("1:schaar");
                Console.WriteLine("2:steen");
                Console.WriteLine("3:papier");
                int keuze = Convert.ToInt32(Console.ReadLine());
                Random rand = new Random();
                int pc = rand.Next(1, 4);

                switch (keuze)
                {
                    case 1:
                        switch (pc)
                        {
                            case 1:
                                Console.WriteLine("gelijkspel");
                                break;
                            case 2:
                                Console.WriteLine("de pc kiest steen, je verliest");
                                scorePC++;
                                break;
                            case 3:
                                Console.WriteLine("de pc kiest papier, je wint");
                                scoreH++;
                                break;
                        }
                        break;
                    case 2:
                        switch (pc)
                        {
                            case 1:
                                Console.WriteLine("de pc kiest schaar, je wint");
                                scoreH++;
                                break;
                            case 2:
                                Console.WriteLine("gelijkspel");
                                break;
                            case 3:
                                Console.WriteLine("de pc kiest papier, je verliest");
                                scorePC++;
                                break;
                        }
                        break;
                    case 3:
                        switch (pc)
                        {
                            case 1:
                                Console.WriteLine("de pc kiest schaar, je verliest");
                                scorePC++;
                                break;
                            case 2:
                                Console.WriteLine("de pc kiest steen, je wint");
                                scoreH++;
                                break;
                            case 3:
                                Console.WriteLine("gelijkspel");

                                break;

                        }
                        break;
                }
                Console.WriteLine($"de score is (jij:pc) {scoreH} : {scorePC}");
            }
            if (scoreH > scorePC) {
                Console.WriteLine("jij wint !");
                
                    }
            else if(scoreH<scorePC) 
            {
                Console.WriteLine("de pc wint !");
            }
            Console.ReadLine();
        }

    }
}
    

