﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TussentijdseTest2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("welke oefening?");
            Console.WriteLine("1");
            Console.WriteLine("2");
            Console.WriteLine("3");
            int keuze = Convert.ToInt32(Console.ReadLine());
            switch (keuze)
            {
                case 1:
                    oefening1();
                    break;
                case 2:
                    oefening2();
                    break;
                case 3:
                    oefening3();
                    break;
            }
           
            

        }

        static void oefening1()
        {
            for (int i = 1; i <= 100; i++)
            {
                Console.Write(i.ToString() + "\t");
                if (i % 10 == 0)
                {
                    Console.WriteLine("\t");
                }

            }
            Console.ReadLine();

        }


        static void oefening2()
        {
            string voornaam = "Stijn";
            string achternaam = "Willemen";
            string lievelingsmuziek = "rap";
            int lenght = achternaam.Length;

            Console.WriteLine($" De lengte van '{voornaam}' is {voornaam.Length}");
            Console.WriteLine($"De lengte van '{achternaam}' is {achternaam.Length}");
            Console.WriteLine($"De lengte van  {lievelingsmuziek} is {lievelingsmuziek.Length}");
            Console.WriteLine($"Eerste karakter in familienaam: {achternaam.Substring(0, 1)}");
            Console.WriteLine($"Tweede karakter in familienaam: {achternaam.Substring(1, 1)}");
            Console.WriteLine($"Eerste karakter in voornaam: {voornaam.Substring(0, 1)}");
            Console.WriteLine($"tweede karakter in familienaam: {achternaam.Substring(1, 1)} het derde karakter is: {achternaam.Substring(2, 1)}");
            Console.WriteLine($"laatste karakter in familienaam: {achternaam.Substring(achternaam.Length - 1, 1)}");
            Console.WriteLine($"je initialen zijn:{voornaam.Substring(0, 1)}.{achternaam.Substring(0, 1)}");
            Console.WriteLine("Naam spellen:");
            for (int n = 0; n <= lenght - 1; n++)
            {
                Console.WriteLine($"\t {achternaam.Substring(n, 1)}");
            }
            {

                string b = ""; int l = (achternaam.Length - 1);
                for (int i = -l; i <= l; i++)
                { b = b + achternaam[l]; l--; }
                Console.WriteLine("omgekeerde achternaam: " + b);

                Console.ReadLine();
            }
        }
        static void oefening3()
        {
            double som = 0;
            int teller = 0;
            int teller2 = 0;

            do
            {


                Console.WriteLine("Geef een getal in tussen 1.0 en 10.0");

                double getal = Convert.ToDouble(Console.ReadLine());

                if (getal >= 1 && getal <= 10)
                {
                    som = som + getal;
                    teller2++;
                }
                else
                {
                    teller++;
                }
                Console.WriteLine("meer getallen invoeren? J of N");

            }
            while (Console.ReadKey(true).Key != ConsoleKey.N);

            Console.WriteLine($"De som van de getallen is {som} en u gaf {teller} aantal foute getallen en {teller2} aantal juiste getallen");
            Console.ReadKey();
        }
    }
}
