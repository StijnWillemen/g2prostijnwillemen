﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoenenverkoper
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Vanaf hoeveel krijge we korting?");
            int korting = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("hoeveel paar schoenen wenst u te kopen?");
            int skoenen = Convert.ToInt32(Console.ReadLine());
            if (skoenen >= korting)
            {
                Console.WriteLine("De prijs bedraagt:" + (skoenen * 10) + " euro");
            }
            else
            {
                Console.WriteLine("De prijs bedraagt: " + (skoenen * 20) + " euro");
            }
            Console.ReadLine();
        }
    }
}
