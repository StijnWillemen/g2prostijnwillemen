﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrieHoekRadialaen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("geef een hoek in graden");
            string hoekgraden = Console.ReadLine();
            double hoekgrade = Convert.ToDouble(hoekgraden);
            double rad = hoekgrade * (Math.PI / 180);
            double rad2=Math.Round(rad, 2);

            Console.WriteLine(rad2);
            double cos = Math.Cos(rad);
            double cos2 = Math.Round(cos,2);
            Console.WriteLine("cos= " + cos2);
            double tan = Math.Tan(rad);
            double tan2 = Math.Round(tan, 2);
            Console.WriteLine("tan= "+ tan2);
            double sin = Math.Sin(rad);
            double sin2 = Math.Round(sin, 2);
            Console.WriteLine("sin= " + sin2);
            Console.Read();
        }
    }
}
