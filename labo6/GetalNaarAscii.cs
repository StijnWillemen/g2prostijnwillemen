﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetallenAlsTekst
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("geef een getal boven 10 in");
            int n = Convert.ToInt32(Console.ReadLine());

            for (int b = 10; n >= b; b++)
            {
                char c=Convert.ToChar(b);
                Console.WriteLine(c);
            }
            Console.ReadLine();
        }
    }
}
