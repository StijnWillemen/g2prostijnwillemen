﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgrammerenLaboLes3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            

            double getal= 45.34598;
            Console.WriteLine($"{getal:C}");

            Console.ForegroundColor = ConsoleColor.Blue;
            string sea = @"                 X
                 X
+----------X----XX------------------------------------------------+
| XXXXX X XX X  X   X                                             |
| X  X                 X  X XX XXXX X X  X X  XX X X  X X XXXXX   |
| X  X    XXX    X   X     X XX              X   X     XX      XXX|
|  X X        XXXX   X  X X X   X XX   X  X  X    XX XXXX    X    |
|  X X  XX XX  XX X XXX      XX  X   XX    XX          XXXX XX    |
|  XXXXXX XXXX XXXXXXXXXXXXX XXX XXX X XXXX X XX X X X   XX XX    |
|   XX XX XXXXX X  X  X  X  X  XX    XX XX X X XXXXX XXXXXX XX    |
|  XXX X  XXXXX XX X XXX    XX  XX    XX   XX  X   X   X  X X     |
|    X X  X  X XX   XX  XXXXXX  X XXXX XX  X XX    X XXX  X   X   |
|  X X XX X X  XXXXXXXXX XX XXX XX XX X X X XXXXXX       X   X    |
|   X  XXXXXXXXX  XXX          X XXXX XX  X   X XX X  X X  X X    |
|    XXX XX  X X  X XX   X X X   XXXXXX XXXXX  X X XXXX   X  XXX  |
|     XXX XXX  XX  XXXXXXX XX   XXXXX  X XX XX X     XX  X   X    |
|  XXXXXX X  X X  X XX X XXXXX XXXX XXXX X X X  X    XXX      XXX |
|    X X         X XX  XX XX X X XXXX XXXXXXX  XXXXXX XXX    XXXX |
|     X X XXX   X X X XXXXXXX X XXX    X XXXXXXXXXXXXXXXXX   XX X |
|   X  XX     XXX  XX XXXXXX XX XX   XX X X   XXXXXX XXXX XX  XX  |
|     XX XXXX       XXXXXX    X XX  XXX  X    XX XXXXXXX      XX  |
|     XX   XX  XXXXXXXXXX  X XXXX XXXXXXXX X XXX XX XXXX  X  X X  |
|   XX X XXXXX X X XXXX    X XXXX     XX X  XX     X      X    X  |
|   XX XX     X X   X     XXXX X     X        X    X X  X         |
+----X--XXXX---X--XXXXXX-X-X--X-----X-----------------------------+
        XX  X   X XXX      X       X    X
          XX       X
           X       X
               XX
";
                Console.WriteLine(sea);


   


            Console.ReadKey();
            
        }
    }
}
