﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H2WeerstandBerekenaarDeel1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("geef waarde eerste ring");
            string Ring1 = Console.ReadLine();
            int Ring1e = Convert.ToInt32(Ring1);
            Console.WriteLine("geef waarde tweede ring");
            string ring2 = Console.ReadLine();
            int Ring2e = Convert.ToInt32(ring2);
            Console.WriteLine("geef waarde derde ring");
            string ring3 = Console.ReadLine();
            int Ring3e = Convert.ToInt32(ring3);
            Console.WriteLine("geef waarde van vierde ring zonder procent");
            string ring4 = Console.ReadLine();
            int Ring4e = Convert.ToInt32(ring4);
            int factor = (Ring1e * 10) + Ring2e;
            int result = (factor * Ring3e);
            Console.WriteLine( $"de waarde is gelijk aan: {result}Ohm met {Ring4e}% tolerantie");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(@" ╔═══════════════╦════════════╦══════════════╦═════════╦");
            Console.WriteLine(@" ║  Ring 1            Ring 2       Ring 3       Ohm    ║");
            Console.WriteLine(@" ╟───────────────╫────────────╫──────────────╫─────────╫");
            Console.WriteLine($@" ║    {Ring1e}             {Ring2e}        {Ring3e}        {result}    ║");
            Console.WriteLine(@" ╚═══════════════╩════════════╩══════════════╩═════════╩");
            Console.ReadLine();



            
            



        }
    }
}
