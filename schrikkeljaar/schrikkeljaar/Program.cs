﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace schrikkeljaar
{
    class Program
    {
        static void Main(string[] args)
        {
            start:
            Console.WriteLine("geef het jaar ");
            int jaar = Convert.ToInt32(Console.ReadLine());
            if (jaar % 4 == 0 && jaar % 100 != 0)
            {
                Console.WriteLine("schrikkeljaar");
            }
            else if (jaar % 4 == 0 && jaar % 100 == 0 && jaar % 400!=0)
            {
                Console.WriteLine("geen schrikkeljaar");
            }
            else if(jaar % 4==0 && jaar % 100==0 && jaar % 400==0)
            {
                Console.WriteLine("schrikkeljaar");
                    
            }
            else if (jaar % 4!=0)
            {
                Console.WriteLine("geen schrikkeljaar");
            }
            Console.ReadKey();
            goto start;
        }
    }
}
